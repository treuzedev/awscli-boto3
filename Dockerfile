FROM ubuntu:focal as tmp

RUN apt-get update --yes && \
    apt-get upgrade --yes && \
    apt-get install --yes --no-install-recommends software-properties-common && \
    apt-add-repository universe && \
    apt-get update --yes && \
    apt-get upgrade --yes && \
    
    apt-get install --yes --no-install-recommends curl ca-certificates unzip && \
    curl -s "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
    unzip awscliv2.zip && \
    ./aws/install && \
    mv /usr/local/bin/aws /usr/bin && \
    
    apt-get install --yes --no-install-recommends python3-pip && \
    pip3 install boto3 --no-cache-dir && \
    
    ln /usr/bin/python3 /usr/bin/python && \
    
    python --version && \
    pip --version && \
    aws --version && \
    
    apt-get remove --yes curl unzip && \
    apt-get clean && rm -rf /var/lib/apt/lists/* && \
    rm -rf aws awscliv2.zip && rm -rf aws
    
COPY files/boto3-test.py /tmp
    
FROM tmp

ENTRYPOINT ["/bin/bash", "-c"]
CMD ["aws --version && python --version && pip freeze | grep boto3"]
